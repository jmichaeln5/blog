<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'PagesController@index');

Route::resource('/applicants', 'ApplicantsController');

Route::get('/sign-up', function () {
    return view('applicants/create');
});

Route::get('/thank-you', 'PagesController@thankyou');

Route::get('/mailing-list', 'ApplicantsController@index');
