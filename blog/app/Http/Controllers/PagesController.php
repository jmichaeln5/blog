<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;

class PagesController extends Controller
{
    //
    public function index(){
        $title = 'Index';
        return view('pages/index')->with('title', $title);
    }

    public function signup(){
        $title = 'Sign Up';
        return view('pages/signup')->with('title', $title);
    }

    public function thankyou(){ 
        return view('pages/thankyou');
    }
}
