<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    // Table Name
    protected $table = 'applicants';

    // Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true; // Set to true by default but can change to false if necessary


}
