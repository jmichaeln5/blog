@extends('layouts/app')

@section('content')

    <h1 class="text-center"> <b> Sign Up </b></h1>

    {!! Form::open(['action' => 'ApplicantsController@store', 'method' => 'POST']) !!}

        <div class="form-group">
            {{Form::label('firstname', 'First Name')}}
            {{Form::text('firstname', '', ['class'=> 'form-control'])}}
            {{-- {{Form::text('firstname', '', ['class'=> 'form-control', 'placeholder' => 'First Name'])}} --}}
        </div>

        <div class="form-group">
            {{Form::label('lastname', 'Last Name')}}
            {{Form::text('lastname', '', ['class'=> 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('email', 'E-mail')}}
            {{Form::text('email', '', ['class'=> 'form-control'])}}
        </div>

        {{Form::submit('Sign me up!', ['class'=> 'btn btn-primary btn-lg btn-block'])}}

    {!! Form::close() !!}

@endsection