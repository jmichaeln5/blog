@extends('layouts/app')

@section('content')
<div class="jumbotron text-center"> 
    <h1> <b>Mailling List.</b></h1>
</div>

    @if(count($applicants) > 0)
    @foreach($applicants as $applicant )

    <div class="card text-center">
        <div class="card-body">
            <h5 class="card-title">{{$applicant->firstname}} {{$applicant->lastname}}</h5>
            <p class="card-text">{{$applicant->email}}</p>
            {{-- <a href="#" class="btn btn-primary">Go somewhere</a>
                <br> 
                <br>  --}}
            <p> Signed up on {{$applicant->created_at}} </p>
        </div>
    </div>
    <br> 
    
    @endforeach  
    @else
        <p> No Applicants Found.</p>
    @endif
    


@endsection