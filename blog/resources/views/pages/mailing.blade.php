@extends('layouts/app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <h1>{{$title}}</h1>
        </div>

        <div class="row justify-content-center">

            @if(count($people) > 0)
            <ul>
                @foreach($people as $person )
                    <li>{{$person}}</li>
                @endforeach
            </ul>
            @endif
            
        </div>
    </div>

@endsection